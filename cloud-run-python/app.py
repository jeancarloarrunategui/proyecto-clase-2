import os
from flask import Flask
from flask import render_template, request
from paciente import Paciente

app = Flask(__name__)


@app.route('/')
def form():
    '''
        @TODO
    '''
    return render_template('form.html')


@app.route('/', methods=['POST'])
def my_form_post():
    '''
        @TODO
    '''
    new_paciente = Paciente(request.form['id'], request.form['nombre'],
                          request.form['apellido_paterno'], request.form['apellido_materno'],
                          request.form['region'], request.form['edad'])

    if new_paciente.insert():
        ingresado = True

    return render_template('form.html',
                           nombre_paciente=new_paciente.nombre,
                           ingresado=ingresado
                           )


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0',
            port=int(os.environ.get(
                     'PORT', 8080)))
